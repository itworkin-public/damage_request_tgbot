from peewee import PostgresqlDatabase, SqliteDatabase

from src.bot.config import BOT_CONFIG, PG_CONFIG


__pg_db = PostgresqlDatabase(
    database=PG_CONFIG.DB,
    user=PG_CONFIG.USER,
    password=PG_CONFIG.PASSWD,
    host=PG_CONFIG.HOST,
    port=PG_CONFIG.PORT,
)
__sl_db = SqliteDatabase(BOT_CONFIG.ROOT_FOLDER / BOT_CONFIG.DB_FILE)

db = __sl_db if BOT_CONFIG.DB_TYPE == "sqlite" else __pg_db
