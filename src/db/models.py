from peewee import Model, CharField, FloatField, IntegerField, TextField

from .base import db


class Base(Model):
    class Meta:
        database = db


class Request(Base):
    request_id = IntegerField(primary_key=True)

    tg_user_id = IntegerField()
    tg_user_phone = CharField(default="-")

    location_addr = CharField(default="-")
    location_longitude = FloatField(default=0)
    location_latitude = FloatField(default=0)
    location_type = CharField(default="-")
    location_flat = CharField(default="-")
    location_room = CharField(default="-")
    building_num = CharField(default="-")

    description = TextField(default="-")


def create_tables():
    db.create_tables([Request])
