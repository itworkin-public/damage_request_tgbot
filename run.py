from asyncio import set_event_loop

from aiogram.types import BotCommand

from src.db.models import create_tables

from src.bot.base import dp, exc, loop
from src.bot.utils import create_static_folder
from src.bot.handlers import *


async def on_startup(_):
    await dp.bot.set_my_commands([BotCommand("start", "Start bot")])


if __name__ == "__main__":
    create_static_folder()
    create_tables()

    set_event_loop(loop)
    exc.start_polling(dp, on_startup=on_startup)
